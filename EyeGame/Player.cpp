#include "Player.h"
#include <SDL/SDL.h>

Player::Player()
{
}


Player::~Player()
{
}

void Player::init(float speed, glm::vec2 pos, GamesEngine::InputManager* inputManager, int screenW, int screenH) {
	
	m_speed = speed;
	m_position = pos;
	m_colour = GamesEngine::ColourRGBA8(255, 255, 255, 255);
	m_inputManager = inputManager;
	m_maxSWdith = screenW;
	m_maxSHeight = screenH;
}

void Player::update() {

	//Set up edges of screen
	int maxXpos = m_maxSWdith;
	int minXpos = 0;
	int maxYpos = m_maxSHeight;
	int minYpos = 0;

	//comment out for testing
	m_position = m_eyeTracker.getAvgPos();
	m_position.y = -m_position.y + m_maxSHeight;

	// to do the smaller screen do - window/monitor then times that number by pos.

	//Can't move object outside edges of screen
	if (m_position.x >= maxXpos - g_ITEM_WIDTH) m_position.x = maxXpos - g_ITEM_WIDTH;
	if (m_position.x <= minXpos) m_position.x = minXpos;
	if (m_position.y >= maxYpos - g_ITEM_WIDTH) m_position.y = maxYpos - g_ITEM_WIDTH;
	if (m_position.y <= minYpos) m_position.y = minYpos;

	
	//std::cout << "X = " << m_eyeTracker.getAvgPos().x << " \n Y = " << m_eyeTracker.getAvgPos().y << std::endl;
	//std::cout << "\n IMAGE X: " << m_position.x << "\t\t IMAGE Y: " << m_position.y << std::endl;

	//uncomment for testing
	/*if (m_inputManager->isKeyDown(SDLK_w)) {
		m_position.y += m_speed;
	}
	else if (m_inputManager->isKeyDown(SDLK_s)) {
		m_position.y -= m_speed;
	}

	if (m_inputManager->isKeyDown(SDLK_a)) {
		m_position.x -= m_speed;
	}
	else if (m_inputManager->isKeyDown(SDLK_d)) {
		m_position.x += m_speed;
	}*/

}
