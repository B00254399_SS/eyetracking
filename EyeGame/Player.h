#pragma once
#include <GamesEngine/EyeTracker.h>
#include <GamesEngine/InputManager.h>
#include "GameItem.h"

class Player : public GameItem
{
private:
	GamesEngine::InputManager* m_inputManager; //keep track of keyboard inputs
	int m_maxSWdith, m_maxSHeight; //screen width and height
	GamesEngine::MyGaze m_eyeTracker; // Keep track of eye gaze
public:
	Player();
	~Player();

	void init(float speed, glm::vec2 pos, GamesEngine::InputManager* inputManager, int screenW, int screenH);
	void update() override;
};

