#pragma once
#include <GamesEngine/SpriteBatch.h>

const float g_ITEM_WIDTH = 60;
const float g_ITEM_RADIUS = g_ITEM_WIDTH / 2.0f;

//class GameItem;

class GameObject
{
protected:
	GamesEngine::ColourRGBA8 m_colour;
	glm::vec2 m_position;
	float m_speed;
public:
	GameObject();
	virtual ~GameObject();
	virtual void update() = 0;
	void draw(GamesEngine::SpriteBatch&  spritebatch);

	bool collideWithObject(GameObject* gameObject);

	//Getter
	glm::vec2 getPosition() const { return m_position; }


};

