#pragma once
#include "GameObject.h"


class GameItem : public GameObject
{
public:
	GameItem();
	~GameItem();
	void init(glm::vec2 pos);
	virtual void update();
};

