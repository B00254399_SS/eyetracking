#include "MainGame.h"
#include <GamesEngine/GamesEngine.h>
#include <GamesEngine/Timing.h>
#include <SDL/SDL.h>
#include <iostream>
#include <random>
#include <ctime>

const float g_MAX_TILE_WIDTH = 60;
const float g_PLAYER_SPEED = 5.0f;
const int g_MAX_OBJECTS = 5;

MainGame::MainGame() :
	m_sWidth(1920),
	m_sHeight(1080),
	m_gameState(GameState::PLAY),
	m_fps(0),
	m_player(nullptr),
	m_playerPos(0.0f, 0.0f)
{
    // Empty 
}

MainGame::~MainGame() {
    // IMPLEMENT THIS!
}

void MainGame::run() {
	initSystems();
	initLevel();
	gameLoop();	
}

void MainGame::initSystems() {
	GamesEngine::init();

	m_window.create("Eye Game", m_sWidth, m_sHeight, 0); // flags -> 0 = normal, 1 = invisible, 2 = Fullscreen, 4 = Borderless
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);

	initShaders();

	m_itemSpritebatch.init();

	// Set up the camera
	m_camera.init(m_sWidth, m_sHeight);
}

void MainGame::initLevel() {

	m_player = new Player();
	m_player->init(g_PLAYER_SPEED, m_playerPos, &m_inputManager, m_sWidth, m_sHeight);

	m_gameItems.push_back(m_player);

	std::mt19937 randomEngine;
	randomEngine.seed(time(nullptr));
	std::uniform_int_distribution<int> randX(100, m_sWidth - g_MAX_TILE_WIDTH);
	std::uniform_int_distribution<int> randY(100, m_sHeight - g_MAX_TILE_WIDTH);

	for (int i = 0; i < g_MAX_OBJECTS; i++) {

		m_gameItems.push_back(new GameItem);
		glm::vec2 pos (randX(randomEngine), randY(randomEngine));
		m_gameItems.back()->init(pos);
	}


	
}

void MainGame::initShaders() {
    // Compile our color shader
    m_textureProgram.compileShaders("Shaders/textureShading.vert", "Shaders/textureShading.frag");
    m_textureProgram.addAttribute("vertexPosition");
    m_textureProgram.addAttribute("vertexColor");
    m_textureProgram.addAttribute("vertexUV");
    m_textureProgram.linkShaders();
}

void MainGame::gameLoop() {
	GamesEngine::FpsLimiter fpsLimiter;
	fpsLimiter.setMaxFPS(60.0f);

	while (m_gameState == GameState::PLAY) {
		fpsLimiter.begin();

		if (m_inputManager.isKeyDown(SDLK_ESCAPE)){
			m_gameState = GameState::EXIT;
			SDL_Quit();
			exit(1);
		}

		m_inputManager.update();

		processInput();
		// Update Human collisions
		for (int i = 0; i < m_gameItems.size(); i++) {
			// Collide with other humans
			for (int j = i + 1; j < m_gameItems.size(); j++) {
				m_gameItems[i]->collideWithObject(m_gameItems[j]);
			}
		}
		m_player->update();
		drawGame();
		m_camera.update();
		m_fps = fpsLimiter.end();
	}
}

void MainGame::processInput() {
    SDL_Event evnt;
    //Will keep looping until there are no more events to process
    while (SDL_PollEvent(&evnt)) {
        switch (evnt.type) {
            case SDL_QUIT:
                // Exit the game here!
                break;
            case SDL_MOUSEMOTION:
                m_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
                break;
            case SDL_KEYDOWN:
                m_inputManager.pressKey(evnt.key.keysym.sym);
                break;
            case SDL_KEYUP:
                m_inputManager.releaseKey(evnt.key.keysym.sym);
                break;
            case SDL_MOUSEBUTTONDOWN:
                m_inputManager.pressKey(evnt.button.button);
                break;
            case SDL_MOUSEBUTTONUP:
                m_inputManager.releaseKey(evnt.button.button);
                break;
        }
    }
}

void MainGame::drawGame() {
    // Set the base depth to 1.0
    glClearDepth(1.0);
    // Clear the color and depth buffer
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_textureProgram.use();

	//Draw code goes here
	glActiveTexture(GL_TEXTURE0);

	//Make sure shader uses texture 0
	GLint textureUniform = m_textureProgram.getUniformLocation("mySampler");
	glUniform1i(textureUniform, 0);

	//Grab the camera matrix
	glm::mat4 projectionMatrix = m_camera.getCameraMatrix();
	GLint pUniform = m_textureProgram.getUniformLocation("P");
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projectionMatrix[0][0]);

	//Begin drawing player
	m_itemSpritebatch.begin();

	//Draw the objects
	for (int i = 0; i < m_gameItems.size(); i++) {
		m_gameItems[i]->draw(m_itemSpritebatch);
	}

	m_itemSpritebatch.end();
	m_itemSpritebatch.renderBatch();


	m_textureProgram.unuse();

    // Swap our buffer and draw everything to the screen!
    m_window.swapBuffer();
}