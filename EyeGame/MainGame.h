#pragma once

#include <GamesEngine/Window.h>
#include <GamesEngine/GLSLProgram.h>
#include <GamesEngine/InputManager.h>
#include <GamesEngine/SpriteBatch.h>
#include <GamesEngine/Camera2D.h>
#include <GamesEngine/EyeTracker.h>
#include "Player.h"

enum class GameState { PLAY, EXIT};

class MainGame
{
public:
    MainGame();
    ~MainGame();

    // Runs the game
    void run();

private:
    //Initializes the core systems
    void initSystems();

    //Initializes the shaders
    void initShaders();

    // Main game loop for the program
    void gameLoop();

    // Handles input processing
    void processInput();

    // Renders the game
    void drawGame();

	//Initializes the level
	void initLevel();

    // Member Variables
	GamesEngine::Window m_window; // The game window
    
	GamesEngine::GLSLProgram m_textureProgram; //The shader program

	GamesEngine::InputManager m_inputManager; // Handles input

	GamesEngine::SpriteBatch m_itemSpritebatch; //Draws the items

	GamesEngine::Camera2D m_camera; // Main Camera

	GamesEngine::MyGaze m_eyeTracker; // EyeTracker

	int m_sWidth, m_sHeight; //screen width and height

	int m_fps; //frames per second

	glm::vec2 m_playerPos;

	GameState m_gameState; //handle for gamestate

	Player* m_player; //player object

	std::vector<GameItem*> m_gameItems; // vector of game items

};

