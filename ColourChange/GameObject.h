#pragma once
#include <GamesEngine/SpriteBatch.h>

const float g_ITEM_WIDTH = 60;
const float g_ITEM_RADIUS = g_ITEM_WIDTH / 2.0f;

class GameItem;
class MovingItem;

class GameObject
{
protected:
	GamesEngine::ColourRGBA8 m_colour;
	glm::vec2 m_position;
	float m_speed;
	int m_maxSWdith, m_maxSHeight; //screen width and height
public:
	GameObject();
	virtual ~GameObject();
	virtual void update(std::vector<GameItem*>& gameItem, std::vector<MovingItem*>& movingItems) = 0;
	void draw(GamesEngine::SpriteBatch&  spritebatch);

	bool collideWithObject(GameObject* gameObject);

	//Getter
	glm::vec2 getPosition() const { return m_position; }
	GamesEngine::ColourRGBA8 getColour() const { return m_colour; }
	
	//Setter 
	void setColour(const GamesEngine::ColourRGBA8 colour) { m_colour = colour; }

};

