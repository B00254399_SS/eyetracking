#pragma once
#include "GameObject.h"


class GameItem : public GameObject
{
public:
	GameItem();
	~GameItem();
	void init(glm::vec2 pos, int screenW, int screenH);
	virtual void update(std::vector<GameItem*>& gameItem, std::vector<MovingItem*>& movingItems);
};

