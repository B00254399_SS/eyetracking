#pragma once

#include "GameObject.h"

class MovingItem : public GameObject
{
private:
	glm::vec2 m_direction;
	int m_frames;
	int m_maxSWdith, m_maxSHeight; //screen width and height
public:
	MovingItem();
	~MovingItem();

	void init(float speed, glm::vec2 pos, int screenW, int screenH);
	virtual void update(std::vector<GameItem*>& gameItem, std::vector<MovingItem*>& movingItems);
};

