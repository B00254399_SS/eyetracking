#include "MovingItem.h"
#include <ctime>
#include <random>
#include <glm/gtx/rotate_vector.hpp>



MovingItem::MovingItem() : m_frames(0)
{
}


MovingItem::~MovingItem()
{
}

void MovingItem::init(float speed, glm::vec2 pos, int screenW, int screenH) {

	static std::mt19937 randomEngine(time(nullptr));
	static std::uniform_real_distribution<float> randDir(-1.0f, 1.0f);

	m_speed = speed;
	m_position = pos;
	m_maxSWdith = screenW;
	m_maxSHeight = screenH;
	m_colour = GamesEngine::ColourRGBA8(250, 12, 83, 255);

	// Get random direction
	m_direction = glm::vec2(randDir(randomEngine), randDir(randomEngine));
	// Make sure direction isn't zero
	if (m_direction.length() == 0) m_direction = glm::vec2(1.0f, 0.0f);

	m_direction = glm::normalize(m_direction);
}


void MovingItem::update(std::vector<GameItem*>& gameItem, std::vector<MovingItem*>& movingItems) {

	static std::mt19937 randomEngine(time(nullptr));
	static std::uniform_real_distribution<float> randRotate(-40.0f, 40.0f);

	m_position += m_direction * m_speed;

	// Randomly change direction every 20 frames
	if (m_frames == 400) {
		m_direction = glm::rotate(m_direction, randRotate(randomEngine));
		m_frames = 0;
	}
	else {
		m_frames++;
	}

	//Set up edges of screen
	int maxXpos = m_maxSWdith;
	int minXpos = 0;
	int maxYpos = m_maxSHeight;
	int minYpos = 0;

	//m_position = m_eyeTracker.getAvgPos();
	//m_position.y = -m_position.y + m_maxSHeight;

	// NOTE:::to do the smaller screen do - window/monitor then times that number by pos.

	//Can't move object outside edges of screen
	if (m_position.x >= maxXpos - g_ITEM_WIDTH) m_direction = glm::rotate(m_direction, randRotate(randomEngine));
	if (m_position.x <= minXpos)  m_direction = glm::rotate(m_direction, randRotate(randomEngine));
	if (m_position.y >= maxYpos - g_ITEM_WIDTH) m_direction = glm::rotate(m_direction, randRotate(randomEngine));
	if (m_position.y <= minYpos)  m_direction = glm::rotate(m_direction, randRotate(randomEngine));

}
