#include "GameObject.h"
#include <GamesEngine/ResourceManager.h>
#include <iostream>

GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

void GameObject::draw(GamesEngine::SpriteBatch&  spritebatch) {

	static int textureID = GamesEngine::ResourceManager::getTexture("Textures/circle.png").m_id;

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	glm::vec4 destRect;
	destRect.x = m_position.x;
	destRect.y = m_position.y;
	destRect.z = g_ITEM_WIDTH;
	destRect.w = g_ITEM_WIDTH;

	spritebatch.draw(destRect, uvRect, textureID, 0.0f, m_colour);
}



bool GameObject::collideWithObject(GameObject* gameObject) {
	// Minimum distance before there is a collision
	const float MIN_DISTANCE = g_ITEM_RADIUS * 2.0f;

	// Center position of this agent
	glm::vec2 centerPosA = m_position + glm::vec2(g_ITEM_RADIUS);
	// Center position of the parameter agent
	glm::vec2 centerPosB = gameObject->getPosition() + glm::vec2(g_ITEM_RADIUS);

	// Distance vector between the two agents
	glm::vec2 distVec = centerPosA - centerPosB;

	// Length of the distance vector
	float distance = glm::length(distVec);

	// Depth of the collision
	float collisionDepth = MIN_DISTANCE - distance;

	// If collision depth > 0 then we did collide
	if (collisionDepth > 0) {
		// Get the direction * the collision depth so we can push them away from each other
		//glm::vec2 collisionDepthVec = glm::normalize(distVec) * collisionDepth * collisionDepth;

		// Push them in opposite directions
		//m_position += collisionDepthVec / 2.0f;
		//gameObject->m_position -= collisionDepthVec / 2.0f;

		//std::cout << "Collision\n";

		return true;
	}
	return false;
}
