#include "GameItem.h"
#include <GamesEngine/ResourceManager.h>
#include <random>
#include <ctime>


GameItem::GameItem()
{
}


GameItem::~GameItem()
{
}

void GameItem::init(glm::vec2 pos, int screenW, int screenH) {

	m_position = pos;
	m_maxSWdith = screenW;
	m_maxSHeight = screenH;
	m_colour = GamesEngine::ColourRGBA8(24, 198, 211, 255);

	//Set up edges of screen
	int maxXpos = m_maxSWdith;
	int minXpos = 0;
	int maxYpos = m_maxSHeight;
	int minYpos = 0;


	// NOTE:::to do the smaller screen do - window/monitor then times that number by pos.

	//Can't move object outside edges of screen
	if (m_position.x >= maxXpos - g_ITEM_WIDTH) m_position.x = maxXpos - g_ITEM_WIDTH;
	if (m_position.x <= minXpos) m_position.x = minXpos;
	if (m_position.y >= maxYpos - g_ITEM_WIDTH) m_position.y = maxYpos - g_ITEM_WIDTH;
	if (m_position.y <= minYpos) m_position.y = minYpos;

}

void GameItem::update(std::vector<GameItem*>& gameItem, std::vector<MovingItem*>& movingItems) {

	

}

