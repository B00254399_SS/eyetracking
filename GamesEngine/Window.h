#pragma once
#include <SDL\SDL.h>
#include <GL\glew.h>
#include<string>
namespace GamesEngine {
	enum WindowFlags { INVISIBLE = 0x1, FULLSCREEN = 0x2, BORDERLESS = 0x4 };

	class Window
	{
	private:
		SDL_Window* m_sdlWindow;
		int m_screenWidth, m_screenHeight;
	public:
		Window();
		~Window();

		int getScreenWidth() { m_screenWidth; }
		int getScreenHeight() { m_screenHeight; }
		int create(std::string windowName, int screenWidth, int screenHeight, unsigned int currentFlags);

		void swapBuffer();
	};
}

