#pragma once
namespace GamesEngine {
	class FpsLimiter {
	private:
		float m_maxFPS, m_frameTime, m_fps;
		unsigned int m_startTicks;
		void calculateFPS();
	public:
		FpsLimiter();
		void init(float maxFPS);
		void setMaxFPS(float maxFPS);
		void begin();
		//end will return current FPS
		float end();

	};
} //end of namespace
