#pragma once
#include "TextureCache.h"
#include <string>
namespace GamesEngine {
	//This is a way for us to access all our resources, such as
	//Models or textures.

	class ResourceManager
	{
	private:
		static TextureCache m_textureCache;

	public:
		static GLTexture getTexture(std::string texturePath);

	};
}

