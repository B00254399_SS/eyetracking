#pragma once

#include <string>
#include <GL/glew.h>
namespace GamesEngine {

	//This class handles the compilation, linking, and usage of a GLSL shader program.
	//Reference: http://www.opengl.org/wiki/Shader_Compilation
	class GLSLProgram
	{
	private:

		int m_numAttributes;
		GLuint m_programID;
		GLuint m_vertexShaderID;
		GLuint m_fragmentShaderID;

		void compileShader(const std::string& filePath, GLuint id);

	public:
		GLSLProgram();
		~GLSLProgram();

		void compileShaders(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilepath);

		void linkShaders();

		void addAttribute(const std::string& attributeName);

		void use();
		void unuse();

		GLuint getUniformLocation(const std::string& uniformName);

	};
}

