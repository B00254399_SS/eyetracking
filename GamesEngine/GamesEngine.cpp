#include<SDL\SDL.h>
#include<GL\glew.h>
#include "GamesEngine.h"

namespace GamesEngine {
	int init() {
		//Initialize SDL
		SDL_Init(SDL_INIT_EVERYTHING);

		//Tell SDL we want a double buffered window to avoid flickering
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		return 0;
	}
}