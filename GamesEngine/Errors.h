#pragma once

#include <string>
namespace GamesEngine {
	//Prints out an error message and exits the game
	extern void fatalError(std::string errorString);
}
