#pragma once
#include <iostream>
#include <EyeTribe/gazeapi.h>
#include <glm/glm.hpp>

// Based on http://dev.theeyetribe.com/cpp/
// g++ tut.cpp -lGazeApiLib -lboost_system -lboost_thread

namespace GamesEngine {
	struct MyGaze : public gtl::IGazeListener
	{
		MyGaze() // Connect to the server in push mode on the default TCP port (6555)
		{
			if (m_api.connect(true)) {
				std::cout << "Connection successful.\n";
				m_api.add_listener(*this); // Enable GazeData notifications
			}
			else {
				std::cout << "Connection NOT successful!\n";
			}
		}
		~MyGaze()
		{
			bool bIsConnected = m_api.is_connected();
			m_api.remove_listener(*this);
			m_api.disconnect();
			std::cout << "\n(" << bIsConnected << ") Disconnected\n";
		}

	private:
		
		void on_gaze_data(gtl::GazeData const &gaze_data)
		{
			const bool bTrackingGaze = gaze_data.state & gtl::GazeData::GD_STATE_TRACKING_GAZE;
			//    if (gaze_data.state & gtl::GazeData::GD_STATE_TRACKING_GAZE)
			if (bTrackingGaze) // All ok, plus we are receiving tracked eye coordinates
			{
				//gtl::Point2D const &coord = gaze_data.avg;	// n.b. averaged

				gtl::Point2D const & smoothedCoordinatesLeftEye = gaze_data.lefteye.avg;	// smoothed data from left eye
				gtl::Point2D const & smoothedCoordinatesRightEye = gaze_data.righteye.avg; // smoothed data from right eye

				m_LeftEyeX = smoothedCoordinatesLeftEye.x;
				m_LeftEyeY = smoothedCoordinatesLeftEye.y;

				m_RightEyeX = smoothedCoordinatesRightEye.x;
				m_RightEyeY = smoothedCoordinatesRightEye.y;

				m_avgPos.x = (m_LeftEyeX + m_RightEyeX) / 2;
				m_avgPos.y = (m_LeftEyeY + m_RightEyeY) / 2;

			}
		}

		gtl::GazeApi m_api;
		float m_LeftEyeX, m_LeftEyeY, m_RightEyeX, m_RightEyeY;
		glm::vec2 m_avgPos;



	public:
		glm::vec2 getAvgPos() { return m_avgPos; }

	};
}
