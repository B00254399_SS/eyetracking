#pragma once
#include <unordered_map>
#include <glm/glm.hpp>

namespace GamesEngine {
class InputManager
{
private:
	std::unordered_map<unsigned int, bool> m_keyMap;
	std::unordered_map<unsigned int, bool> m_previousKeyMap;
	glm::vec2 m_mouseCoords;

	// Returns true if the key was just pressed
	bool wasKeyDown(unsigned int keyID);
public:
	InputManager();
	~InputManager();

	void update();

	void pressKey(unsigned int keyID);
	void releaseKey(unsigned int keyID);

	// Returns true if the key is held down
	bool isKeyDown(unsigned int keyID); 

	// Returns true if the key was just pressed
	bool isKeyPressed(unsigned int keyID); 

	//setters
	void setMouseCoords(float x, float y);

	//getters
	glm::vec2 getMouseCoords() const { return m_mouseCoords; }
};
}

