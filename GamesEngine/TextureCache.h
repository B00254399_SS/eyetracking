#pragma once
#include <map>
#include"GLTexture.h"
namespace GamesEngine {
	//This caches the textures so that multiple sprites can use the same textures
	class TextureCache
	{
	private:
		std::map<std::string, GLTexture> _textureMap;
	public:
		TextureCache();
		~TextureCache();

		GLTexture getTexture(std::string texturePath);
	};
}

