#pragma once
#include <GL/glew.h>
#include "GLTexture.h"

#include <string>
namespace GamesEngine {
	//A 2D quad that can be rendered to the screen
	class Sprite
	{
	private:
		float m_x, m_y, m_width, m_height;
		GLuint m_vboID;
		GLTexture m_texture;

	public:
		Sprite();
		~Sprite();

		void init(float x, float y, float width, float height, std::string texturePath);
		void draw();
	};
}

