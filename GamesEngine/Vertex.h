#pragma once

#include <GL/glew.h>
namespace GamesEngine {
	//This is the position struct, makes more sense to do it this way rather than position[2]
	struct Position { //Position is the type
		float x, y;
	};


	struct ColourRGBA8 {
		ColourRGBA8() : r(0), g(0), b(0), a(0) { } //empty default constructer, set to black

		ColourRGBA8(GLubyte R, GLubyte G, GLubyte B, GLubyte A) :
								r(R), g(G), b(B), a(A) { } //empty, constructer to set to whatever the inputs are 
		GLubyte r, g, b, a;
	};


	struct UV {
		float u, v;
	};
	//vertex definition
	struct Vertex {
		Position position;

		//4 bytes for RGBA colour
		ColourRGBA8 colour;

		//UV texture coordinates
		UV uv;

		void setColour(GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
			colour.r = r;
			colour.g = g;
			colour.b = b;
			colour.a = a;
		}

		void setUV(float u, float v) {
			uv.u = u;
			uv.v = v;
		}

		void setPosition(float x, float y) {
			position.x = x;
			position.y = y;
		}
	};
}
