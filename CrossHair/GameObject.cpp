#include "GameObject.h"
#include <GamesEngine/ResourceManager.h>
#include <iostream>

GameObject::GameObject()
{
}


GameObject::~GameObject()
{
}

void GameObject::draw(GamesEngine::SpriteBatch&  spritebatch) {

	static int textureID = GamesEngine::ResourceManager::getTexture("Textures/circle.png").m_id;

	const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

	glm::vec4 destRect;
	destRect.x = m_position.x;
	destRect.y = m_position.y;
	destRect.z = g_ITEM_WIDTH;
	destRect.w = g_ITEM_WIDTH;

	spritebatch.draw(destRect, uvRect, textureID, 0.0f, m_colour);
}

